//
//  Person.m
//  Categories
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person.h"

@implementation Person
@synthesize name = _name;
@synthesize friends = _friends;

-(id)init{
    self = [super init];
    if(self){
        _friends = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)sayHello {
    NSLog(@"Hello, says %@.", _name);
}

- (void)sayGoodbye {
    NSLog(@"Goodbye, says %@.", _name);
}

@end
