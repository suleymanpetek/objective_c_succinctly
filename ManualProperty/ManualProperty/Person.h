//
//  Person.h
//  ManualProperty
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
{
    unsigned int _age;
}
@property unsigned int age;
@end
