//
//  Person.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person.h"

@implementation Person
@synthesize name = _name;

-(void)dealloc{
    [_name release];
    _name = nil;
    NSLog(@"Dealloc called");
    [super dealloc];
}
@end
