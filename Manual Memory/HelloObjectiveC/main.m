//
//  main.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // Claim the object
        Person *frank = [[Person alloc] init];
        
        // Use the object
        frank.name = @"Frank";
        NSLog(@"%@", frank.name);
        
        // Free the object
        [frank release];
        
        return 0;
        
    }
    return 0;
}

